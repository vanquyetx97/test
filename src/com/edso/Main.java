package com.edso;



public class Main {

    public static void main(String[] args) {

        Producer producer = new Producer();
        producer.setName("Producer : ");
        producer.start();

        Consumer consumer = new Consumer(producer);
        consumer.setName("Consumer : ");
        consumer.start();
    }
}
