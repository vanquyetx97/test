package com.edso;

public class Consumer extends Thread {

    private final Producer producer;

    public Consumer(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String time = producer.consume();
                System.out.println("Consumer by: " + Thread.currentThread().getName() + " Thời gian tạo: " + time);
                Thread.sleep(3000);

            }
        } catch (Exception exp) {
        }
    }
}
