package com.edso;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Producer extends Thread {

    private static final int MAX_SIZE = 3;
    private final List<String> messages = new ArrayList<>();

    @Override
    public void run() {
        try {
            while (true) {
                produce();
            }
        } catch (Exception exp) {
        }
    }

    private synchronized void produce() throws Exception {
        while (messages.size() == MAX_SIZE) {
            System.out.println("Đã đạt đến giới hạn hàng đợi.");
            wait(3000);
            System.out.println("Producer đã nhận được thông báo từ người tiêu dùng");
        }
        String time = LocalDateTime.now().toString();
        messages.add(time);
        System.out.println("Producer đã tạo ra dữ liệu ");
        notify();
    }

    public synchronized String consume() throws Exception {
        notify();
        while (messages.isEmpty()) {
            wait();
        }
        String time = messages.get(0);
        messages.remove(time);
        return time;
    }
}
